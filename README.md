


### Implemented Functionality
 Ask for geolocation pemission, detect the geolocation of the user device, and show the current weather and forecast.

 This project uses Openweather API as the data source. Unfortunately, Openweather does not provide pollen/pollen count information.
 Pulls the location weather from an API Openweather for a list of public weather APIs that you could use
 Allows the user to switch between Imperial/Metric units
 Allows the user to enter in a location name to look up
 Add dynamic changing weather images


### Functionality Choices

React hooks are widely used. All functional component, no class component
consistent index.js import and export: default import/export for Components, named import/export for variables , Helper files and Service files for data formating and API calling.


### Dev Settings
React bootstrap has been used
React JS ^18.2.0
jest + React Testing Library for debugging in Chrome