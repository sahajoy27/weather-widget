import React from 'react';
import { Col } from 'react-bootstrap';

export const ForecastCard = ({ data, index, handleChangeForecast }) => {

    console.log(JSON.stringify(data))

    return (
        <Col className='forecast-container' onClick={() => handleChangeForecast(index)} data-testid={`forecast_test_${index}`}>
            <div className='forecast'>
                <span className='weather-text' data-testid="day">{index === 0 ? 'Today' : new Date(data?.date).toLocaleDateString("en-US", { weekday: 'long' })}</span>
                <img src={data?.img_url} alt={data?.img_name} className='weather-forecast-img' />
                <span aria-label="temperature" data-testid="temperature">{Math.round(data?.temp)}°</span>
            </div>
        </Col>
    )
}
