import { render, screen, fireEvent } from '@testing-library/react';
import { ForecastCard } from './ForecastCard';

test('renders the forecast card', () => {

    const weather_data = {
        "city": {
            "id": 8131499,
            "name": "Konkan Division",
            "coord": {
                "lat": 19.076,
                "lon": 72.8777
            },
            "country": "IN",
            "population": 0,
            "timezone": 19800,
            "sunrise": 1658104803,
            "sunset": 1658152135
        },
        "forecast": [{
            "date": "7/18/2022",
            "current_temp": 79,
            "description": "Moderate rain",
            "feels_like": 79,
            "wind_speed": 10,
            "temp": 79,
            "wind": {
                "speed": 9.86,
                "deg": 240,
                "gust": 14.27
            },
            "img_url": "http://openweathermap.org/img/wn/10n@2x.png"
        }, {
            "date": "7/19/2022",
            "current_temp": 79,
            "description": "Light rain",
            "feels_like": 79,
            "wind_speed": 8,
            "temp": 79,
            "wind": {
                "speed": 8.48,
                "deg": 252,
                "gust": 12.06
            },
            "img_url": "http://openweathermap.org/img/wn/10n@2x.png"
        }, {
            "date": "7/20/2022",
            "current_temp": 80,
            "description": "Light rain",
            "feels_like": 85,
            "wind_speed": 14,
            "temp": 80,
            "wind": {
                "speed": 14.45,
                "deg": 247,
                "gust": 18.14
            },
            "img_url": "http://openweathermap.org/img/wn/10n@2x.png"
        }, {
            "date": "7/21/2022",
            "current_temp": 80,
            "description": "Light rain",
            "feels_like": 85,
            "wind_speed": 15,
            "temp": 80,
            "wind": {
                "speed": 15.08,
                "deg": 259,
                "gust": 18.61
            },
            "img_url": "http://openweathermap.org/img/wn/10n@2x.png"
        }, {
            "date": "7/22/2022",
            "current_temp": 82,
            "description": "Light rain",
            "feels_like": 87,
            "wind_speed": 11,
            "temp": 82,
            "wind": {
                "speed": 11.32,
                "deg": 246,
                "gust": 15.28
            },
            "img_url": "http://openweathermap.org/img/wn/10n@2x.png"
        }, {
            "date": "7/23/2022",
            "current_temp": 81,
            "description": "Light rain",
            "feels_like": 87,
            "wind_speed": 13,
            "temp": 81,
            "wind": {
                "speed": 13.18,
                "deg": 273,
                "gust": 16.62
            },
            "img_url": "http://openweathermap.org/img/wn/10n@2x.png",
            "img_name": "10n"

        }]
    };
    /**Test Data */
    const data = {
        "date": "7/18/2022",
        "current_temp": 79,
        "description": "Moderate rain",
        "feels_like": 79,
        "wind_speed": 10,
        "temp": 79,
        "wind": {
            "speed": 9.86,
            "deg": 240,
            "gust": 14.27
        },
        "img_url": "http://openweathermap.org/img/wn/10n@2x.png"
    };

    const index = 0;
    const handleChangeForecast = jest.fn(() => {
        return {
            "date": "7/13/2022",
            "current_temp": 26,
            "description": "Heavy intensity rain",
            "feels_like": 26,
            "wind_speed": 10,
            "temp": 26,
            "wind": {
                "speed": 10.39,
                "deg": 240,
                "gust": 13.62
            },
            "img_url": "http://openweathermap.org/img/wn/10n@2x.png"
        };
    })
    render(<ForecastCard  {...{ data, index, handleChangeForecast }} />);
    const temperature_element = screen.getByTestId('temperature');
    expect(temperature_element).toHaveTextContent('79');
    const day = screen.getByTestId('day');
    expect(day).toHaveTextContent('Today');
    fireEvent.click(screen.getByTestId('forecast_test_0'));
    expect(handleChangeForecast).toHaveBeenCalled();
});
