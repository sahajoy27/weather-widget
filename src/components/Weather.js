import React from 'react';
import { useEffect, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { getWeatherApiData, getLocationData } from "../services/Services";
import { formateApiData } from "../utils/Helper";
import Spinner from 'react-bootstrap/Spinner';

import { WeatherCard } from './WeatherCard';
export const Weather = () => {

    const [weather_data, setWeatherData] = useState();
    const [latitude, setLat] = useState(null);
    const [longitude, setLong] = useState(null);
    const [city_data, setCityData] = useState(null);
    const [city, setCity] = useState('');
    const [units, setUnit] = useState('metric');
    const [param_state, setParamState] = useState('');
    const [current_weather_data, setCurrentWeatherData] = useState({});
    const [error, setError] = useState('Enable location access to browser or serach a City!');


    const handleChangeInput = (e) => {
        setCity(e.target.value);
        getLocationData(e.target.value).then((res) => {
            if (res.status === 200) {
                setCityData(res?.data);
                setParamState('active');
            }
        });

    }
    const handleChangeOfSelect = (e) => {
        setLat(e?.target?.dataset?.lat);
        setLong(e?.target?.dataset?.lon);
        setParamState('');
        setCityData(null);
        setCity('');

    }
    const handleClick = (e) => setParamState('');


    useEffect(() => {

        /**Set latitude and longitude data*/
        if (latitude === null && longitude === null) {

            navigator?.geolocation?.getCurrentPosition(function (position) {
                setLat(position?.coords?.latitude);
                setLong(position?.coords?.longitude);
                setError();

            });
        } else {
            getWeatherApiData(latitude, longitude, units).then((result) => {
                if (result?.status === 200) {
                    /**Format Api Data */
                    const res = formateApiData(result);
                    setWeatherData({ city: res.city, forecast: res?.forecast_data });
                    setCurrentWeatherData(res?.forecast_data[0]);
                } else {
                    setError(result?.message);
                }
            });
        }

    }, [latitude, longitude, units, error])



    return (
        <div className='weather-widget'>
            <Row>
                <Col>
                    <Container fluid onClick={handleClick}>
                        <Row className="justify-content-md-center" >
                            <Col>
                                <input type="text" value={city} onChange={handleChangeInput} className="form-control input-lg" placeholder='Enter City Name' aria-label="city" data-testid="location_test" />
                                <ul className={`country-list dropdown-menu ${param_state}`} aria-labelledby="country">
                                    {
                                        city_data && city_data.map((value, key) => {
                                            return <li key={key} data-lat={value?.lat} data-lon={value?.lon} onClick={(e) => { handleChangeOfSelect(e) }} className="country-list-item dropdown-item" >{value?.name}  {value?.state ? ', ' + value?.state : ''}</li>
                                        })
                                    }
                                </ul>
                            </Col>
                        </Row>

                        {weather_data ? <WeatherCard {...{ weather_data, units, setUnit, current_weather_data, setCurrentWeatherData }} /> :
                            <Row >
                                <span style={{ textAlign: 'center', paddingTop: '20px' }}>{error ? error :
                                    <div>
                                        <Spinner animation="grow" variant="info" />
                                        <Spinner animation="grow" variant="info" />
                                        <Spinner animation="grow" variant="info" />
                                    </div>}
                                </span>
                            </Row>}
                    </Container>
                </Col>
            </Row>
        </div>


    )

}
