import { render, screen, fireEvent } from '@testing-library/react';
import { Weather } from './Weather';

test('renders the Weather widget', () => {
    render(<Weather />);
    const handleChangeInput = jest.fn((e) => {
        return e.target.value;
    });
    fireEvent.change(screen.getByTestId('location_test'), { target: { value: 'Delhi' } });
    expect(screen.getByTestId('location_test').value).toBe('Delhi');
});





