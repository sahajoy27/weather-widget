import React from 'react';
import { ForecastCard } from './ForecastCard';
import { Row, Col } from 'react-bootstrap';

export const WeatherCard = ({ weather_data, units, setUnit, current_weather_data, setCurrentWeatherData }) => {


    const handleChangeForecast = (key) => {
        setCurrentWeatherData(weather_data?.forecast[key]);
    }

    return (
        <>
            <Row className='pt-3'>
                <Col>
                    <span className='city-name'><h3>{weather_data && weather_data?.city?.name}, {weather_data?.city?.country}</h3></span>
                    <div className='weather-text'>
                        <span>{current_weather_data && new Date(current_weather_data?.date).toLocaleDateString("en-US", { weekday: 'long', day: 'numeric', month: 'long' })}</span><br />
                        <span>{current_weather_data?.description}</span>
                    </div>
                </Col>
            </Row>
            <Row className="justify-content-md-center">
                <Col xs={3}>
                    <div className='weather-temp'>
                        <img src={current_weather_data?.img_url} alt="weather" className='weather-img' />
                        <span aria-label="temperature" className='temperature'>{current_weather_data?.current_temp}</span>
                        <span aria-label="°Celsius" className={units === 'metric' ? 'temp-unit active' : 'temp-unit'} data-unit="metric" onClick={(e) => setUnit(e?.target?.dataset?.unit)} data-testid="celcius">°C </span> | <span aria-label="°Fahrenheit" className={units === 'imperial' ? 'temp-unit active' : 'temp-unit'} data-unit="imperial" onClick={(e) => setUnit(e?.target?.dataset?.unit)} data-testid="fahrenheit">°F</span>
                    </div>
                </Col>
                <Col>
                    <div className='weather-text right'>
                        <span aria-label="feels like" data-testid="feels_like">Feels Like: {current_weather_data?.feels_like}°</span><br />
                        <span aria-label="humidity" data-testid="humidity_text">Humidity: {current_weather_data?.humidity}%</span><br />
                        <span aria-label="speed" data-testid="wind_text">
                            Wind: {current_weather_data?.wind_speed}
                            {units === 'metric' ? ' Kph' : ' Mph'}
                        </span><br />
                    </div>
                </Col>
            </Row>
            <Row className="forecast-card">
                {weather_data && weather_data?.forecast?.map((data, index) => {
                    return (
                        <ForecastCard key={Math.floor(new Date(data?.date).getTime() / 1000)} {...{ data, index, handleChangeForecast }} />
                    )
                })}
            </Row>
        </>
    )
}
