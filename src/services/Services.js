import axios from "axios";
import { APP_API_URL, APP_API_KEY } from "../constants";

export const getWeatherApiData = (latitude, longitude, units) => {

    /**Set latitude and longitude data */
    const endpoint = `${APP_API_URL}/data/2.5/forecast?id=524901&units=${units}&lat=${latitude}&lon=${longitude}&appid=${APP_API_KEY}`;

    return axios.get(endpoint).then(result => {
        return result;
    }).catch(error => { return error });
}

export const getLocationData = (city) => {

    const endpoint = `${APP_API_URL}/geo/1.0/direct?q=${city}&limit=5&appid=${APP_API_KEY}`;

    return axios.get(endpoint).then(result => {
        return result;
    }).catch(error => { return error });
}

