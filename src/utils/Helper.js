import { APP_Img_URL } from "../constants";

export const formateApiData = (result) => {

    let filter_forecast_data = [];
    const formated_data = {};

    formated_data.city = result?.data?.city;
    /**Filter relavent forecast data based on dates */
    result?.data?.list.forEach((data) => {
        const date = new Date(data?.dt * 1000).toLocaleDateString("en-US");
        const formated_data = {
            date: new Date(data?.dt * 1000).toLocaleDateString("en-US"),
            current_temp: Math.round(data.main?.temp),
            description: data?.weather[0]?.description.charAt(0).toUpperCase() + data?.weather[0]?.description.slice(1),
            feels_like: Math.round(data?.main?.feels_like),
            wind_speed: Math.round(data?.wind?.speed),
            temp: Math.round(data?.main?.temp),
            wind: data?.wind,
            img_url: APP_Img_URL + data?.weather[0]?.icon + "@2x.png",
            img_name: data?.weather[0]?.icon
        }

        if (filter_forecast_data.length === 0) {


            filter_forecast_data.push(formated_data);
        } else {

            let state = false;
            filter_forecast_data.forEach((item) => {
                return state = (date !== item?.date) ? true : false;

            }
            )
            if (state) {
                filter_forecast_data.push(formated_data);
            }
        }
    })
    formated_data.forecast_data = filter_forecast_data;
    return formated_data;
}